using System;
using CommonProtocolLibrary;

public interface INetWindow{
    void Send(string Data);
    void SetRecv(IProtocol SetProtocol, Action<bool> networkState);
    void Connection(string uri, Action<bool> networkState);
    void Disconnect();
}
