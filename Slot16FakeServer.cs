﻿using System;
using System.Collections.Generic;
using CommonProtocolLibrary;
using UnityEngine;
using MathClassLibrary;
using SlotProtocol.WayGame;
using SlotProtocol;
using Slot16;

public class Slot16FakeServer : MonoBehaviour, INetWindow
{

    public static Slot16FakeServer inst;

    Action<string> callBackToStateReceive;

    public int[][] nowSymbol = ShowSymbol;

    Slot16Math slot16mathModel = new Slot16Math();

    long playerMoney;

    enum FakeMath
    {
        /// <summary>
        /// 數學模型
        /// </summary>
        Math,
        /// <summary>
        /// 只有MainGame
        /// </summary>
        MG,
        /// <summary>
        /// 非洲人
        /// </summary>
        MG_None,
        /// <summary>
        /// 只有一次FreeGame
        /// </summary>
        FG_0,
        /// <summary>
        /// FreeGame最大上限
        /// </summary>
        FG_2
    }


    [SerializeField]
    FakeMath _FakeMath;


    private void Awake()
    {
        inst = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            GameInfo gameInfo = new GameInfo();

            //gameInfo = slot16mathModel.Spin(50, 1);

            ShowMessage(gameInfo);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            GameInfo gameInfo = new GameInfo();

            int[] bbb = new int[] { 17, 15, 14, 14, 17 };

         //   gameInfo = slot16mathModel.ESpin(bbb, 50, 1);

            ShowMessage(gameInfo);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
          //  slot16mathModel.SendPlayerChoice(0);
        }
    }

    public void SetCallback(Action<string> _callback)
    {
        callBackToStateReceive = _callback;
    }

    public void Connection(string uri, Action<bool> networkState)
    {
        throw new NotImplementedException();
    }

    public void Disconnect()
    {
        throw new NotImplementedException();
    }

    public void Send(string Data)
    {
        throw new NotImplementedException();
    }

    public void SetRecv(IProtocol SetProtocol, Action<bool> networkState)
    {
        throw new NotImplementedException();
    }

    public void Revceive(string Data)
    {
        string code = Data.Substring(0, 5);
        //AsgardDebugger.Log(Data);
        string description = Data.Substring(4);

        string result = EatPacket(code, description);

        callBackToStateReceive.Invoke(result);
    }

    private string EatPacket(string code, string description)
    {
        string result = "";
        // 依收到頻率高低排列
        switch (code)
        {
            case C2S_CommonOpcode.RoleJoinGame + "{":

                break;
            case C2S_CommonOpcode.RoleExitGame + "{":

                break;
            case C2S_CommonOpcode.RoleChat + "{":

                break;
            case WayGame_C2S.PlayerInfo + "{":
                result = S2C_PlayerInfo();
                break;
            case WayGame_C2S.Spin + "{":
                result = S2C_Spin(description);
                symbolIndex += 1;
                if (symbolIndex == nowSymbol.Length)
                    symbolIndex = 0;
                break;
            case WayGame_C2S.ChooseFG + "{":
                result = S2C_Choose(description);
                break;
            default:
                break;
        }
        return result;
    }

    string S2C_PlayerInfo()
    {
        S2C_PlayerInfo s2C_PlayerInfo = new S2C_PlayerInfo()
        {
            ErrorCode = 0,
            NickName = "QAQ",
            PlayerMoney = 1000000,
            Diamond = 500,
            PlayerLevel = 300,
            PlayerExp = 0
        };

        playerMoney = s2C_PlayerInfo.PlayerMoney;

        return CreateStringWithObjAndCode(S2C_ChooseOneSlotGame.PlayerInfo, s2C_PlayerInfo);
    }

    string S2C_Spin(string _des)
    {
        C2S_Spin spin = new C2S_Spin();
        JsonUtility.FromJsonOverwrite(_des, spin);

        if (_FakeMath == FakeMath.Math)
        {
            Fn_CheckFG(spin.Bet, spin.Gold);
            //進數學模型
            GameInfo gameInfo = new GameInfo();
           // gameInfo = slot16mathModel.Spin(spin.Bet, spin.Gold);

            ShowMessage(gameInfo);

            S2C_Spin s2C_Spin = new S2C_Spin();
            s2C_Spin = SetData(gameInfo);

            return CreateStringWithObjAndCode(S2C_ChooseOneSlotGame.Spin, s2C_Spin);
        }
        else if (_FakeMath == FakeMath.MG)
        {
            Fn_CheckFG(spin.Bet, spin.Gold);
            nowSymbol = ShowSymbol;
            //進數學模型
            GameInfo gameInfo = new GameInfo();
           // gameInfo = slot16mathModel.ESpin(nowSymbol[symbolIndex], spin.Bet, spin.Gold);


            S2C_EngineerSpin s2C_Spin = new S2C_EngineerSpin();
            s2C_Spin = SetData_engineer(gameInfo);

            return CreateStringWithObjAndCode(S2C_ChooseOneSlotGame.Spin, s2C_Spin);
        }

        else if (_FakeMath == FakeMath.FG_0)
        {
            Fn_CheckFG(spin.Bet, spin.Gold);

            nowSymbol = FG_0Symbol;
            //進數學模型
            GameInfo gameInfo = new GameInfo();
            //gameInfo = slot16mathModel.ESpin(nowSymbol[symbolIndex], spin.Bet, spin.Gold);


            S2C_EngineerSpin s2C_Spin = new S2C_EngineerSpin();
            s2C_Spin = SetData_engineer(gameInfo);

            return CreateStringWithObjAndCode(S2C_ChooseOneSlotGame.Spin, s2C_Spin);
        }

        else if (_FakeMath == FakeMath.MG_None)
        {
            Fn_CheckFG(spin.Bet, spin.Gold);
            nowSymbol = MG_None;
            //進數學模型
            GameInfo gameInfo = new GameInfo();
        //    gameInfo = slot16mathModel.ESpin(nowSymbol[symbolIndex], spin.Bet, spin.Gold);


            S2C_EngineerSpin s2C_Spin = new S2C_EngineerSpin();
            s2C_Spin = SetData_engineer(gameInfo);

            return CreateStringWithObjAndCode(S2C_ChooseOneSlotGame.Spin, s2C_Spin);
        }
        else
        {
            Fn_CheckFG(spin.Bet, spin.Gold);
            nowSymbol = FG_2Symbol;
            //進數學模型
            GameInfo gameInfo = new GameInfo();
           // gameInfo = slot16mathModel.ESpin(nowSymbol[symbolIndex], spin.Bet, spin.Gold);


            S2C_EngineerSpin s2C_Spin = new S2C_EngineerSpin();
            s2C_Spin = SetData_engineer(gameInfo);

            return CreateStringWithObjAndCode(S2C_ChooseOneSlotGame.Spin, s2C_Spin);
        }
    }

    string S2C_Choose(string _data)
    {
        C2S_Choose c2S_Choose = new C2S_Choose();

        JsonUtility.FromJsonOverwrite(_data, c2S_Choose);

       // slot16mathModel.SendPlayerChoice(c2S_Choose.Choosen);

        S2C_Choose s2C_Choose = new S2C_Choose()
        {
            ErrorCode = 0
        };

        return CreateStringWithObjAndCode(S2C_ChooseOneSlotGame.ChooseFG, s2C_Choose);
    }

    string CreateStringWithObjAndCode(string opcode, object data)
    {
        AsgardDebugger.Log(opcode + JsonUtility.ToJson(data));
        return opcode + JsonUtility.ToJson(data);
    }

    #region 一般模式
    /// <summary>
    /// 設定給客端的資料
    /// </summary>
    /// <param name="gameInfo"></param>
    /// <returns></returns>
    S2C_Spin SetData(GameInfo _gameInfo)
    {
        long FGWinPoint = Fn_GetFGWinPoint(_gameInfo);

        S2C_Spin s2C_spin = new S2C_Spin()
        {
            ErrorCode = 0,
            Symbol = _gameInfo.Symbol,
            WinlineInfo = SetS2CLineInfoArray(_gameInfo.WinLineInfo),
            ScatterWinInfo = SetS2CLineInfo(_gameInfo.ScatterWinInfo),
            FGWinInfo = SetS2CLineInfo(_gameInfo.FGWinInfo),
            BGWinInfo = SetS2CLineInfo(_gameInfo.BonusWinInfo),
            Adjust = _gameInfo.AdjustmentValue,
            TotalWin = _gameInfo.TotalWin.ToString(),
            PlayerMoney = playerMoney + (long)_gameInfo.TotalWin + FGWinPoint
        };
        playerMoney = s2C_spin.PlayerMoney;
        return s2C_spin;
    }

    /// <summary>
    /// 轉換中線資料
    /// </summary>
    /// <param name="_lineInfo"></param>
    /// <returns></returns>
    S2CLineInfo SetS2CLineInfo(LineInfo _lineInfo)
    {

        if (_lineInfo != null)
        {
            S2CLineInfo _s2CLineInfo = new S2CLineInfo()
            {
                Symbol = _lineInfo.Symbol,
                Count = _lineInfo.Count,
                LineCount = _lineInfo.LineCount,
                LineIndex = _lineInfo.LineIndex,
                WinPoint = _lineInfo.WinPoint.ToString()
            };
            return _s2CLineInfo;
        }
        else
        {
            S2CLineInfo _s2CLineInfo = new S2CLineInfo()
            {
                Symbol = -1,
            };
            return _s2CLineInfo;
        }

    }

    /// <summary>
    /// 轉換中線資料(陣列)
    /// </summary>
    /// <param name="_lineInfo"></param>
    /// <returns></returns>
    S2CLineInfo[] SetS2CLineInfoArray(LineInfo[] _lineInfo)
    {
        if (_lineInfo != null)
        {
            List<S2CLineInfo> temp = new List<S2CLineInfo>();

            for (int count = 0; count < _lineInfo.Length; count++)
            {
                S2CLineInfo _s2CLineInfo = new S2CLineInfo()
                {
                    Symbol = _lineInfo[count].Symbol,
                    Count = _lineInfo[count].Count,
                    LineCount = _lineInfo[count].LineCount,
                    LineIndex = _lineInfo[count].LineIndex,
                    WinPoint = _lineInfo[count].WinPoint.ToString()
                };
                temp.Add(_s2CLineInfo);
            }

            return temp.ToArray();
        }
        else
        {
            return null;
        }
    }
    #endregion

    #region 工程模式
    /// <summary>
    /// 設定給客端的資料
    /// </summary>
    /// <param name="gameInfo"></param>
    /// <returns></returns>
    S2C_EngineerSpin SetData_engineer(GameInfo _gameInfo)
    {
        long FGwinPoint = Fn_GetFGWinPoint(_gameInfo);

        S2C_EngineerSpin s2C_spin = new S2C_EngineerSpin()
        {
            ErrorCode = 0,
            Symbol = _gameInfo.Symbol,
            WinlineInfo = SetS2CLineInfoArray_engineer(_gameInfo.WinLineInfo),
            ScatterWinInfo = SetS2CLineInfo_engineer(_gameInfo.ScatterWinInfo),
            FGWinInfo = SetS2CLineInfo_engineer(_gameInfo.FGWinInfo),
            BGWinInfo = SetS2CLineInfo_engineer(_gameInfo.BonusWinInfo),
            Adjust = _gameInfo.AdjustmentValue,
            TotalWin = _gameInfo.TotalWin.ToString(),
            PlayerMoney = playerMoney + (long)_gameInfo.TotalWin + FGwinPoint
        };
        playerMoney = s2C_spin.PlayerMoney;
        return s2C_spin;
    }

    /// <summary>
    /// 轉換中線資料
    /// </summary>
    /// <param name="_lineInfo"></param>
    /// <returns></returns>
    S2CLineInfo SetS2CLineInfo_engineer(LineInfo _lineInfo)
    {

        if (_lineInfo != null)
        {
            S2CLineInfo _s2CLineInfo = new S2CLineInfo()
            {
                Symbol = _lineInfo.Symbol,
                Count = _lineInfo.Count,
                LineCount = _lineInfo.LineCount,
                LineIndex = _lineInfo.LineIndex,
                WinPoint = _lineInfo.WinPoint.ToString()
            };
            return _s2CLineInfo;
        }
        else
        {
            S2CLineInfo _s2CLineInfo = new S2CLineInfo()
            {
                Symbol = -1,
            };
            return _s2CLineInfo;
        }

    }

    /// <summary>
    /// 轉換中線資料(陣列)
    /// </summary>
    /// <param name="_lineInfo"></param>
    /// <returns></returns>
    S2CLineInfo[] SetS2CLineInfoArray_engineer(LineInfo[] _lineInfo)
    {
        if (_lineInfo != null)
        {
            List<S2CLineInfo> temp = new List<S2CLineInfo>();

            for (int count = 0; count < _lineInfo.Length; count++)
            {
                S2CLineInfo _s2CLineInfo = new S2CLineInfo()
                {
                    Symbol = _lineInfo[count].Symbol,
                    Count = _lineInfo[count].Count,
                    LineCount = _lineInfo[count].LineCount,
                    LineIndex = _lineInfo[count].LineIndex,
                    WinPoint = _lineInfo[count].WinPoint.ToString()
                };
                temp.Add(_s2CLineInfo);
            }

            return temp.ToArray();
        }
        else
        {
            return null;
        }
    }
    #endregion

    void Fn_CheckFG(int _bet, int _gold)
    {
       // AsgardDebugger.Log(slot16mathModel.GetGameStatus());
        //AsgardDebugger.Log("MathFreeSpinTimes: "+slot16mathModel.GetNowFreeSpin());
        //if (slot16mathModel.GetGameStatus() != GameStatus.FreeGame)
        //{
        //    playerMoney -= _bet * _gold;
        //    //AsgardDebugger.Log(playerMoney);
        //}
    }

    long Fn_GetFGWinPoint(GameInfo _gameInfo)
    {
        
        long temp = 0;

        if (_gameInfo.FGWinInfo == null)
        {
            temp = 0;
        }
        else
        {
            temp = _gameInfo.FGWinInfo.WinPoint;
        }
        return temp;
    }


    // 測試腳本區
    int symbolIndex = 0;

    #region [假資料設定區]
    static int[][] ShowSymbol = new int[][]
    {
        // 0條線
        new int[]
        {
            1,2,3,4,5,
            1,2,3,4,5,
            1,2,3,4,5
        },

        // 1條線
        new int[]
        {
            5,5,0,5,5,
            1,2,3,4,4,
            1,2,3,4,5
        },

        // 3條線
        new int[]
        {
            1,1,1,1,6,
            2,2,2,6,4,
            3,3,6,4,4
        },
                new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,1,1,1,1,
            1,1,1,1,1,
            1,1,1,1,1
        },
    };
    static int[][] MG_None = new int[][]
    {
        new int[]
        {
            1,2,3,4,5,
            1,2,3,4,5,
            1,2,3,4,5
        },
    };

    static int[][] FG_0Symbol = new int[][]
    {
        new int[]
        {
            12,12,12,12,7,
            1,1, 1, 9, 9,
            2,2, 2, 2, 5
        },
        // 0條線
        new int[]
        {
            1,2,3,4,5,
            1,2,3,4,5,
            1,2,3,4,5
        },

        // 1條線
        new int[]
        {
            5,5,0,5,5,
            1,2,3,4,4,
            1,2,3,4,5
        },

        // 3條線
        new int[]
        {
            1,1,1,1,6,
            2,2,2,6,4,
            3,3,6,4,4
        },
        new int[]
        {
            1,1,1,1,1,
            1,1,1,1,1,
            1,1,1,1,1
        },
       new int[]
        {
            7,0,0,0,7,
            8,0,0,0,8,
            9,9,9,0,9
        },
        new int[]
        {
            10,11,1,2,3,
            7,5,7,1,3,
            6,6,6,6,6
        },
        new int[]
        {
            1,1,1,1,1,
            2,2,2,2,2,
            3,3,4,5,6
        },
        new int[]
        {
            1,1,0,0,1,
            2,0,2,2,2,
            3,3,3,3,3
        },
        new int[]
        {
            7,0,0,0,7,
            8,0,0,0,8,
            9,9,9,0,9
        },
        new int[]
        {
            10,11,1,2,3,
            9,5,7,1,3,
            6,6,6,6,6
        },
        new int[]
        {
            1,1,1,1,1,
            2,2,2,2,2,
            3,3,4,5,6
        },
        new int[]
        {
            1,1,0,0,1,
            2,0,2,2,2,
            3,3,3,3,3
        },
        new int[]
        {
            7,0,0,0,7,
            8,0,0,0,8,
            9,9,9,0,9
        },
        new int[]
        {
            10,11,1,2,3,
            5,5,7,1,3,
            6,6,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
    };

    static int[][] FG_2Symbol = new int[][]
    {
        new int[]
        {
            12,12,12,12,7,
            1,1, 1, 9, 9,
            2,2, 2, 2, 5
        },
       new int[]
        {
            12,12,12,12,7,
            3,1, 1, 9, 9,
            5,2, 2, 2, 5
        },
        // 0條線
        new int[]
        {
            1,2,3,4,5,
            1,2,3,4,5,
            1,2,3,4,5
        },

        // 1條線
        new int[]
        {
            5,5,0,5,5,
            1,2,3,4,4,
            1,2,3,4,5
        },

        // 3條線
        new int[]
        {
            1,1,1,1,6,
            2,2,2,6,4,
            3,3,6,4,4
        },
        new int[]
        {
            1,1,1,1,1,
            1,1,1,1,1,
            1,1,1,1,1
        },
       new int[]
        {
            7,0,0,0,7,
            8,0,0,0,8,
            9,9,9,0,9
        },
        new int[]
        {
            10,11,1,2,3,
            7,5,7,1,3,
            6,6,6,6,6
        },
        new int[]
        {
            1,1,1,1,1,
            2,2,2,2,2,
            3,3,4,5,6
        },
        new int[]
        {
            1,1,0,0,1,
            2,0,2,2,2,
            3,3,3,3,3
        },
        new int[]
        {
            7,0,0,0,7,
            8,0,0,0,8,
            9,9,9,0,9
        },
        new int[]
        {
            10,11,1,2,3,
            9,5,7,1,3,
            6,6,6,6,6
        },
        new int[]
        {
            1,1,1,1,1,
            2,2,2,2,2,
            3,3,4,5,6
        },
        new int[]
        {
            1,1,0,0,1,
            2,0,2,2,2,
            3,3,3,3,3
        },
        new int[]
        {
            7,0,0,0,7,
            8,0,0,0,8,
            9,9,9,0,9
        },
        new int[]
        {
            10,11,1,2,3,
            5,5,7,1,3,
            6,6,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        // 0條線
        new int[]
        {
            1,2,3,4,5,
            1,2,3,4,5,
            1,2,3,4,5
        },

        // 1條線
        new int[]
        {
            5,5,0,5,5,
            1,2,3,4,4,
            1,2,3,4,5
        },

        // 3條線
        new int[]
        {
            1,1,1,1,6,
            2,2,2,6,4,
            3,3,6,4,4
        },
        new int[]
        {
            1,1,1,1,1,
            1,1,1,1,1,
            1,1,1,1,1
        },
       new int[]
        {
            7,0,0,0,7,
            8,0,0,0,8,
            9,9,9,0,9
        },
        new int[]
        {
            10,11,1,2,3,
            7,5,7,1,3,
            6,6,6,6,6
        },
        new int[]
        {
            1,1,1,1,1,
            2,2,2,2,2,
            3,3,4,5,6
        },
        new int[]
        {
            1,1,0,0,1,
            2,0,2,2,2,
            3,3,3,3,3
        },
        new int[]
        {
            7,0,0,0,7,
            8,0,0,0,8,
            9,9,9,0,9
        },
        new int[]
        {
            10,11,1,2,3,
            9,5,7,1,3,
            6,6,6,6,6
        },
        new int[]
        {
            1,1,1,1,1,
            2,2,2,2,2,
            3,3,4,5,6
        },
        new int[]
        {
            1,1,0,0,1,
            2,0,2,2,2,
            3,3,3,3,3
        },
        new int[]
        {
            7,0,0,0,7,
            8,0,0,0,8,
            9,9,9,0,9
        },
        new int[]
        {
            10,11,1,2,3,
            5,5,7,1,3,
            6,6,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
                new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
        new int[]
        {
            1,2,1,2,3,
            5,5,7,1,3,
            6,3,6,6,6
        },
    };
    #endregion

    //#region [免費轉區]

    //static int[][] freeSymbols = new int[][]
    //{
    //    new int[]
    //    {
    //        1,1,1,1,1,
    //        2,2,2,2,2,
    //        3,3,4,5,6
    //    },
    //    new int[]
    //    {
    //        1,1,0,0,1,
    //        2,0,2,2,2,
    //        3,3,3,3,3
    //    },
    //    new int[]
    //    {
    //        7,0,0,0,7,
    //        8,0,0,0,8,
    //        9,9,9,0,9
    //    },
    //    new int[]
    //    {
    //        10,11,1,2,3,
    //        1,5,7,1,3,
    //        6,6,6,6,6
    //    },
    //    new int[]
    //    {
    //        7,8,9,10,1,
    //        5,12,12,12,5,
    //        2,2,4,7,8
    //    },
    //    new int[]
    //    {
    //        12,12,12,1,1,
    //        2,2,2,2,2,
    //        3,3,4,5,6
    //    },
    //    new int[]
    //    {
    //        1,1,0,0,1,
    //        2,0,2,2,2,
    //        3,3,3,3,3
    //    },
    //    new int[]
    //    {
    //        7,0,0,0,7,
    //        8,0,0,0,8,
    //        9,9,9,0,9
    //    },
    //    new int[]
    //    {
    //        10,11,1,2,3,
    //        1,5,7,1,3,
    //        6,6,6,6,6
    //    },
    //    new int[]
    //    {
    //        1,1,1,1,1,
    //        2,2,2,2,2,
    //        3,3,4,5,6
    //    },
    //    new int[]
    //    {
    //        1,1,0,0,1,
    //        2,0,2,2,2,
    //        3,3,3,3,3
    //    },
    //    new int[]
    //    {
    //        7,0,0,0,7,
    //        8,0,0,0,8,
    //        9,9,9,0,9
    //    },
    //    new int[]
    //    {
    //        10,11,1,2,3,
    //        5,5,7,1,3,
    //        6,6,6,6,6
    //    },
    //    new int[]
    //    {
    //        1,1,1,1,1,
    //        2,2,2,2,2,
    //        3,3,4,5,6
    //    },
    //    new int[]
    //    {
    //        1,1,0,0,1,
    //        2,0,2,2,2,
    //        3,3,3,3,3
    //    },
    //    new int[]
    //    {
    //        7,0,0,0,7,
    //        8,0,0,0,8,
    //        9,9,9,0,9
    //    },
    //    new int[]
    //    {
    //        10,11,1,2,3,
    //        7,5,7,1,3,
    //        6,6,6,6,6
    //    },
    //    new int[]
    //    {
    //        1,1,1,1,1,
    //        2,2,2,2,2,
    //        3,3,4,5,6
    //    },
    //    new int[]
    //    {
    //        1,1,0,0,1,
    //        2,0,2,2,2,
    //        3,3,3,3,3
    //    },
    //    new int[]
    //    {
    //        7,0,0,0,7,
    //        8,0,0,0,8,
    //        9,9,9,0,9
    //    },
    //    new int[]
    //    {
    //        10,11,1,2,3,
    //        9,5,7,1,3,
    //        6,6,6,6,6
    //    },
    //    new int[]
    //    {
    //        1,1,1,1,1,
    //        2,2,2,2,2,
    //        3,3,4,5,6
    //    },
    //    new int[]
    //    {
    //        1,1,0,0,1,
    //        2,0,2,2,2,
    //        3,3,3,3,3
    //    },
    //    new int[]
    //    {
    //        7,0,0,0,7,
    //        8,0,0,0,8,
    //        9,9,9,0,9
    //    },
    //    new int[]
    //    {
    //        10,11,1,2,3,
    //        5,5,7,1,3,
    //        6,6,6,6,6
    //    },
    //};
    //#endregion

    #region [測試用區]
    void ShowMessage(GameInfo _gameInfo)
    {
        string logmessage = "";

        //for (int i = 0; i < _gameInfo.RNGRecord.Length; i++)
        //{
        //    logmessage += "reel " + i + "  rng = " + slot16mathModel.RNGRecord[i];
        //    logmessage += "\n";
        //    //AsgardDebugger.Log("reel " + i + "  rng = " + slot16mathModel.RNGRecord[i]);
        //}
        logmessage += "\n";
        string sym = "";

        for (int i = 0; i < _gameInfo.Symbol.Length; i++)
        {
            if (i % 5 == 0)
            {
                sym += "\n";
            }
            sym += _gameInfo.Symbol[i].ToString();
            sym += ", ";
        }

        //AsgardDebugger.Log(sym);
        logmessage += sym;


        if (_gameInfo.WinLineInfo != null)
        {
            for (int i = 0; i < _gameInfo.WinLineInfo.Length; i++)
            {
                //AsgardDebugger.Log("獎圖編號 " + _gameInfo.WinLineInfo[i].Symbol +
                //    " 共贏了 " + _gameInfo.WinLineInfo[i].LineCount +
                //    " 條 " + (_gameInfo.WinLineInfo[i].Count + 1) +
                //    " 連線 ,共贏 " + _gameInfo.WinLineInfo[i].WinPoint + " 分" +
                //    ", 此次倍數為" + _gameInfo.AdjustmentValue);

                logmessage += "獎圖編號 " + _gameInfo.WinLineInfo[i].Symbol +
                    " 共贏了 " + _gameInfo.WinLineInfo[i].LineCount +
                    " 條 " + (_gameInfo.WinLineInfo[i].Count + 1) +
                    " 連線 ,共贏 " + _gameInfo.WinLineInfo[i].WinPoint + " 分" +
                    ", 此次倍數為" + _gameInfo.AdjustmentValue + "\n";
            }
        }
        //AsgardDebugger.Log(logmessage);
    }
    #endregion
}
